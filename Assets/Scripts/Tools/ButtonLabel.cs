namespace UnityEngine.UI
{
    public class ButtonLabel : Button
    {
        [SerializeField] private Text _label;

        public string Text
        {
            get { return _label.text; }
            set
            {
                if (value == null) value = string.Empty;
                if (_label.text != value)
                {
                    _label.text = value;
                    _label.CalculateLayoutInputHorizontal();
                }
            }
        }
        
        public int TextFontSize
        {
            set => _label.fontSize = value;
        }
    }
}