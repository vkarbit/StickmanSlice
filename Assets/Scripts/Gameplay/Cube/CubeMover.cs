using UnityEngine;

public class CubeMover : MonoBehaviour
{
    [SerializeField] private float _leftBoundary = -2.5f;
    [SerializeField] private float _rightBoundary = 2.5f;
    [SerializeField] private float _movementSpeed = 1f;

    [SerializeField] private bool _movingRight = true;

    private void Update()
    {
        float direction = _movingRight ? 1f : -1f;
        transform.Translate(Vector3.right * direction * _movementSpeed * Time.deltaTime);

        if (transform.position.x >= _rightBoundary)
        {
            _movingRight = false;
        }
        else if (transform.position.x <= _leftBoundary)
        {
            _movingRight = true;
        }
    }
}
