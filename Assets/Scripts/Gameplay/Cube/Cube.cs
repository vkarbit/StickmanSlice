using BzKovSoft.ObjectSlicer.Samples;
using System;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public bool HasCut { get; private set; }
    public Action OnCut;
    public ObjectSlicerSample ObjectSlicerSample { get; private set; }
    private Rigidbody _rigidbody;
    private CubeMover _cubeMover;
    private void Start()
    {
        HasCut = false;

        ObjectSlicerSample = GetComponent<ObjectSlicerSample>();
        _rigidbody = GetComponent<Rigidbody>();
        _cubeMover = GetComponent<CubeMover>();
    }

    public void Cut()
    {
        gameObject.layer = 0;
        _cubeMover.enabled = false;
        ObjectSlicerSample.enabled = false;
        _rigidbody.isKinematic = false;

        HasCut = true;
        OnCut?.Invoke();
    }
}
