using DG.Tweening;
using UnityEngine;

public class PlayerAnimation
{
    private const string ANIM_NAME = "Throw";
    private Animator _animator;
    public PlayerAnimation(Animator animator)
    {
        _animator = animator;
    }

    public void PlayThrowAnimation()
    {
        _animator.SetBool(ANIM_NAME, true);
        AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        float currentAnimationLength = stateInfo.length;

        DOVirtual.DelayedCall(currentAnimationLength + 1f, () => _animator.SetBool(ANIM_NAME, false));
    }
}
