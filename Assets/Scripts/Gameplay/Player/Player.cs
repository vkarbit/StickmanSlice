using DG.Tweening;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private BezierTest _bezierTest;
    [SerializeField] private Transform _cardTransform;
    [SerializeField] private Transform _bezierPointsContainer;
    [SerializeField] private Transform _playerHandTransform;
    [SerializeField] private CardSlicer _cardSlicer;
    private Quaternion _cardRotate;

    private PlayerAnimation _playerAnimation;
    private GameService _gameService;
    private StatisticsService _statisticsService;
    public void Initialize(GameService gameService, StatisticsService statisticsService)
    {
        _gameService = gameService;
        _statisticsService = statisticsService;

        _playerAnimation = new PlayerAnimation(_animator);

        _bezierTest.Initialize(_cardTransform, gameService);
        _cardSlicer.Init(statisticsService);
        _cardRotate = _cardTransform.localRotation;
    }

    private void Update()
    {
        _bezierTest.UpdateBezier();

        if(_gameService.CanDoThrow() && _bezierTest.IsMoving == false && Input.GetMouseButtonUp(0)) 
        {
            _playerAnimation.PlayThrowAnimation();
        }
    }

    public void ThrowCard()
    {
        _gameService.DoThrows();
        _bezierTest.transform.SetParent(_bezierPointsContainer);
        _bezierTest.IsMoving = true;
        _bezierTest.T = 1;

        DOTween.To(() => _bezierTest.T, x => _bezierTest.T = x, 0, 0.7f)
            .SetEase(Ease.Linear)
            .OnComplete(() => {
                _bezierTest.IsMoving = false;
                _cardTransform.SetParent(_playerHandTransform);
                _cardTransform.transform.localPosition = Vector3.zero;
                _cardTransform.transform.localRotation = _cardRotate;
                });

    }
}
