﻿using UnityEngine;

public class BezierTest : MonoBehaviour
{
    [SerializeField] private Transform _p0;
    [SerializeField] private Transform _p1;
    [SerializeField] private Transform _p2;
    [SerializeField] private Transform _p3;

    [HideInInspector] public float T;

    [HideInInspector] public bool IsMoving ;

    [SerializeField] private float minX = -10;
    [SerializeField] private float maxX = 10;
    [SerializeField] private float minZ = -5;
    [SerializeField] private float maxZ = 5;
    [SerializeField] private LineRenderer _lineRenderer;
    private Transform _objToMove;
    private GameService _gameService;

    public void Initialize(Transform objToMove, GameService gameService)
    {
        ResetWay();

        _objToMove = objToMove;
        _gameService = gameService;
    }

    public void UpdateBezier()
    {
        if (_gameService.CanDoThrow())
        {
            if (IsMoving == false && Input.GetMouseButton(0))
            {
                Vector2 touchPosition = Input.mousePosition;

                Vector3 clampedPosition = new Vector3(
                    Mathf.Clamp((touchPosition.x / 100) - (Screen.width / 2.0f) / 100, minX, maxX),
                    _p0.transform.position.y,
                    Mathf.Clamp(touchPosition.y / 100, minZ, maxZ)
                );

                _p1.position = clampedPosition;
                _p2.position = clampedPosition;

                UpdateLineRenderer();
            }
            else if (Input.GetMouseButton(0) == false)
            {
                _lineRenderer.gameObject.SetActive(false);
            }
        }

        if (IsMoving)
        {
            _objToMove.position = Bezier.GetPoint(_p0.position, _p1.position, _p2.position, _p3.position, T);
            _objToMove.rotation = Quaternion.LookRotation(Bezier.GetFirstDerivative(_p0.position, _p1.position, _p2.position, _p3.position, T));
            _lineRenderer.gameObject.SetActive(false);
        }
    }


    private void OnDrawGizmos()
    {
        int segmentsNumber = 20;
        Vector3 previousPoint = _p0.position;

        for (int i = 0; i < segmentsNumber + 1; i++)
        {
            float parameter = (float)i / segmentsNumber;
            Vector3 point = Bezier.GetPoint(_p0.position, _p1.position, _p2.position, _p3.position, parameter);
            Gizmos.DrawLine(previousPoint, point);
            previousPoint = point;
        }
    }

    private void UpdateLineRenderer()
    {
        _lineRenderer.gameObject.SetActive(true);
        int segmentsNumber = 20;
        _lineRenderer.positionCount = segmentsNumber + 1;

        for (int i = 0; i < segmentsNumber + 1; i++)
        {
            float parameter = (float)i / segmentsNumber;
            Vector3 point = Bezier.GetPoint(_p0.position, _p1.position, _p2.position, _p3.position, parameter);
            _lineRenderer.SetPosition(i, point);
        }
    }

    private void ResetWay()
    {
        _p1.position = new Vector3(0, 0, (_p0.position.z + _p3.position.z) * 0.5f);
        _p2.position = new Vector3(0, 0, (_p0.position.z + _p3.position.z) * 0.5f);
    }

}
