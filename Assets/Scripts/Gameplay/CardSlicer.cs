using UnityEngine;

public class CardSlicer : MonoBehaviour
{
    private const string LAYER_TO_CUT = "Cut";

    private StatisticsService _statisticsService;
    public void Init(StatisticsService statisticsService)
    {
        _statisticsService = statisticsService;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer(LAYER_TO_CUT))
        {
            Cut(other.gameObject);
            _statisticsService.Statistics.AddCutObj();
        }
    }

    private void Cut(GameObject target)
    {
        var cube = target.GetComponent<Cube>();
        cube.Cut();

        Plane plane = new Plane(Vector3.up, -transform.position.y);
        cube.ObjectSlicerSample.Slice(plane, null);
    }
}
