using System.Collections.Generic;
using UnityEngine;

public class Location : MonoBehaviour
{
    [SerializeField] private List<Cube> _cubes;
    private GameService _gameService;
    private bool _allCubesCut = true;

    public void Initialize(GameService gameService)
    {
        _gameService = gameService;

        gameService.GetLocation(this);

        foreach (var cube in _cubes)
        {
            cube.OnCut += CheckCubesAfterCut;
        }
    }

    private void OnDestroy()
    {
        foreach (var cube in _cubes)
        {
            cube.OnCut -= CheckCubesAfterCut;
        }
    }

    public bool CheckCubesOnEndThrows()
    {
        foreach (var cube in _cubes)
        {
            if (cube.HasCut == false) return false;
        }

        return true;
    }
    public void CheckCubesAfterCut()
    {
        foreach (var cube in _cubes)
        {
            if (!cube.HasCut)
            {
                _allCubesCut = false;
                break;
            }
        }

        if (_allCubesCut)
        {
            _gameService.EndLevel();

            return;
        }

        _allCubesCut = true;
    }
}
