public enum FruitsEnum
{
    None = 0,
    Apple = 1,
    Strawberry = 2,
    Watermelon = 3,
}
