public enum WindowName
{
    None = 0,
    WinWindow = 1,
    StatisticsWindow = 2,
    LoseWindow = 3,
}