using System.Collections.Generic;
using UnityEngine;

public static class RandomEnum
{
    public static T GetRandomNonZeroEnumValue<T>()
    {
        System.Array values = System.Enum.GetValues(typeof(T));
        T randomValue;

        do
        {
            randomValue = (T)values.GetValue(Random.Range(1, values.Length));
        } while (EqualityComparer<T>.Default.Equals(randomValue, default(T)));

        return randomValue;
    }

}
