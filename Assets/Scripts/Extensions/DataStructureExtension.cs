﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Extensions
{
    public static class DataStructureExtension
    {
        private static readonly System.Random _random = new();

        public static T GetRandom<T>(this List<T> list, List<T> excluded)
        {
            List<T> possibleElements = new List<T>();

            foreach (var element in list)
                if (!excluded.Contains(element))
                    possibleElements.Add(element);
            
            int randomIndex = Random.Range(0, possibleElements.Count - 1);
            
            return possibleElements[randomIndex];
        }

        public static T GetRandom<T>(this List<T> list)
        {
            int randomIndex = Random.Range(0, list.Count);

            return list[randomIndex];
        }

        public static List<T> Shuffle<T>(this IEnumerable<T> list)
        {
            List<T> returnList = list.ToList();
            int n = returnList.Count;
            
            while (n > 1)
            {
                n--;
                int k = _random.Next(n + 1);
                (returnList[k], returnList[n]) = (returnList[n], returnList[k]);
            }

            return returnList;
        }

        public static void Replace<T>(this List<T> list, T replaceable, T replacer) => 
            list[list.IndexOf(replaceable)] = replacer;
        
        public static (int, Transform) GetTupleWithMaxInt(this List<(int, Transform)> tupleList)
        {
            var tupleWithMaxInt = tupleList.Aggregate((t1, t2) => t1.Item1 > t2.Item1 ? t1 : t2);
            return tupleWithMaxInt;
        }
        
        public static Dictionary<TKey, List<TValue>> DeepCopy<TKey, TValue>(
            this Dictionary<TKey, List<TValue>> originalDictionary)
        {
            Dictionary<TKey, List<TValue>> copiedDictionary = new Dictionary<TKey, List<TValue>>();

            foreach (var kvp in originalDictionary)
            {
                List<TValue> originalList = kvp.Value;
                List<TValue> copiedList = new List<TValue>(originalList); // Create a new list object
                copiedDictionary.Add(kvp.Key, copiedList);
            }

            return copiedDictionary;
        }
    }
}