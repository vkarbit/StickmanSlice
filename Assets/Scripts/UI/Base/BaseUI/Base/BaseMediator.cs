using System;
using Base;

namespace UI
{
    public abstract class BaseMediator : ServicesInjector
    {
        protected readonly OneListener _shown = new OneListener();
        public event Action Shown
        {
            add => _shown.Add(value);
            remove => _shown.Remove(value);
        }
        
        protected readonly OneListener _hided = new OneListener();
        public event Action Hided
        {
            add => _hided.Add(value);
            remove => _hided.Remove(value);
        }
        
        protected HandlersHolder Handlers;
        
        public void Initialize(BaseView view, Services services)
        {
            base.Inject(services);
            Handlers = new HandlersHolder(services);
            RegisterView(view);
            BindHandlers();
            Handlers.Initialize();
            Mediate();
        }

        private void RegisterView(BaseView view)
        {
            view.Shown += _shown.SafeInvoke;
            view.HideEnd += _hided.SafeInvoke;
            
            OnSetView(view);
        }

        protected abstract void Mediate();

        protected virtual void BindHandlers() { }
        
        protected abstract void OnSetView(BaseView view);

        public void SetData(BaseData data)
        {
            OnSetData(data);
        }
        
        protected abstract void OnSetData(BaseData data);
        
        public abstract void Show();

        public abstract void Hide();

        public abstract void UnMediate();
    }
    
    public abstract class BaseMediator<T> : BaseMediator
        where T : Enum
    {
        public T Name { get; private set; }

        public void SetName(T name)
        {
            Name = name;
        }
    }

    public abstract class BaseMediator<T, TView> : BaseMediator<T>
        where T : Enum
        where TView : BaseView
    {
        
        public T Name { get; private set; }
        protected TView View { get; private set; }

        public void SetName(T name)
        {
            Name = name;
        }

        protected sealed override void OnSetView(BaseView view)
        {
            View = view as TView;
        }

        protected sealed override void Mediate()
        {
            OnMediate();
        }

        protected virtual void OnMediate()
        {
        }

        public sealed override void Show()
        {
            View.Show();
            OnShow();
        }

        protected virtual void OnShow()
        {
        }

        public sealed override void Hide()
        {
            if (View != null) View.Hide();
            OnHide();
        }

        protected virtual void OnHide()
        {
        }

        public sealed override void UnMediate()
        {
            OnUnMediate();
        }

        protected virtual void OnUnMediate()
        {
        }
    }
}