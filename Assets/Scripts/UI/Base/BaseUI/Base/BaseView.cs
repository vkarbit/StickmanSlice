using System;
using UnityEngine;

namespace UI
{
    public abstract class BaseView : MonoBehaviour
    {
        private readonly OneListener _showEnd = new OneListener();
        public event Action Shown
        {
            add => _showEnd.Add(value);
            remove => _showEnd.Remove(value);
        }
        
        private readonly OneListener _hideEnd = new OneListener();
        public event Action HideEnd
        {
            add => _hideEnd.Add(value);
            remove => _hideEnd.Remove(value);
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
            SetAsLastSibling();
            OnShowStart();
        }

        protected virtual void SetAsLastSibling()
        {
            transform.SetAsLastSibling();
        }

        protected virtual void OnShowStart()
        {
            OnShowEnd();
        }

        protected virtual void OnShowEnd()
        {
            _showEnd.SafeInvoke();
        }

        public virtual void Hide()
        {
            OnStartHide();
        }

        protected virtual void OnStartHide()
        {
            OnHideEnd();
        }

        protected virtual void OnHideEnd()
        {
            _hideEnd.SafeInvoke();
            
            if (gameObject != null)
                gameObject.SetActive(false);
        }
    }
}