namespace UI
{
    public abstract class HudView : BaseView
    {
        public sealed override void Show()
        {
            base.Show();
        }

        public sealed override void Hide()
        {
            base.Hide();
        }
    }
}