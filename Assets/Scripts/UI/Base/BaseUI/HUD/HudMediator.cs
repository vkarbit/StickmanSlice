namespace UI
{
    public abstract class HudMediator : BaseMediator
    {
        public HUDName Name { get; private set; }
        
        public void SetName(HUDName name)
        {
            Name = name;
        }
    }

    public abstract class HudMediator<T> : HudMediator
        where T : BaseView
    {
        protected T View { get; private set; }

        protected sealed override void OnSetView(BaseView view)
        {
            View = view as T;
        }
        
        protected override void OnSetData(BaseData data)
        {
        }

        protected sealed override void Mediate()
        {
            OnMediate();
        }

        protected virtual void OnMediate() { } 

        public sealed override void Show()
        {
            View.Show();
            OnShow();
        }

        protected virtual void OnShow() { }

        public sealed override void Hide()
        {
            if (View != null) View.Hide();
            OnHide();
        }
        
        protected virtual void OnHide() { }

        public sealed override void UnMediate()
        {
            OnUnMediate();
        }

        protected virtual void OnUnMediate()
        {
            
        }
    }

    public abstract class HudMediator<T, TD> : HudMediator<T>
        where T : BaseView
        where TD : BaseData
    {
        protected TD Data { get; private set; }
        
        protected sealed override void OnSetData(BaseData data)
        {
            Data = data as TD;
        }
    }
}