﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public abstract class WindowView : BaseView
    {
        public enum eTypeAnimationShow
        {
            None = 0,
            BottomToCenter = 1,
        }
        public enum eTypeAnimationFade
        {
            None = 0,
            CenterToBottom = 1,
        }

        private readonly OneListener _onCloseButtonClick = new OneListener();
        public event Action CloseClick
        {
            add => _onCloseButtonClick.Add(value);
            remove => _onCloseButtonClick.Remove(value);
        }

        [SerializeField] protected Button _closeButton;
        [SerializeField] protected eTypeAnimationShow _showAnimationType;
        [SerializeField] protected eTypeAnimationFade _fadeAnimationType;

        private RectTransform _rectTransform;

        private Vector3 _positionOutsideScreen = new Vector3(0, -1140, 0);

        private const float TransitionDuration = 0.2f;


        public sealed override void Show()
        {
            if (_closeButton != null)
                _closeButton.onClick.AddListener(_onCloseButtonClick.SafeInvoke);

            base.Show();

            if (_rectTransform == null)
            {
                _rectTransform = GetComponent<RectTransform>();
            }

            ShowWindowFromBottom();
        }

        public sealed override void Hide()
        {
            if (_closeButton != null)
                _closeButton.onClick.RemoveAllListeners();

            HideWindowToBottom();

        }

        public void ShowWindowFromBottom()
        {
            if (_showAnimationType == eTypeAnimationShow.None)
            {
                _rectTransform.localPosition = Vector3.zero;
                return;
            }

            switch (_showAnimationType)
            {
                case eTypeAnimationShow.BottomToCenter:
                    if (!_rectTransform) return;
                    _rectTransform.localPosition = _positionOutsideScreen;
                    _rectTransform.DOLocalMoveY(0, TransitionDuration * 1.5f).SetEase(Ease.OutCubic).OnComplete(() =>
                    {
                        _rectTransform.localPosition = Vector3.zero;
                    });
                    break;
            }


        }

        public void HideWindowToBottom()
        {
            if (_fadeAnimationType == eTypeAnimationFade.None)
            {
                base.Hide();
                return;
            }

            switch (_fadeAnimationType)
            {
                case eTypeAnimationFade.CenterToBottom:
                    _rectTransform.localPosition = Vector3.zero;
                    _rectTransform.DOLocalMoveY(-2140, TransitionDuration * 1.5f).SetEase(Ease.OutCubic).OnComplete(() =>
                    {
                        _rectTransform.localPosition = _positionOutsideScreen;
                        base.Hide();
                    });
                    break;
            }
        }
    }
}