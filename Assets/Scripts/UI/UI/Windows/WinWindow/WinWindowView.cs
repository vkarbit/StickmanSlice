using UI;
using UnityEngine;
using UnityEngine.UI;

public class WinWindowView : WindowView
{
    [SerializeField] private Button _returnBackButton;
    public Button ReturnBackButton => _returnBackButton;
}
