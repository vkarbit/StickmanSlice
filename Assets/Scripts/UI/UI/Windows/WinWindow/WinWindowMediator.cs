using UI;

public class WinWindowMediator : WindowMediator<WinWindowView, WindowData>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.ReturnBackButton.onClick.AddListener(OnReturnBackButtonClick);
    }
    protected override void OnShow()
    {
        base.OnShow();

        Services.Statistics.Statistics.AddWin();
    }
    protected override void OnUnMediate()
    {
        base.OnUnMediate();

        View.ReturnBackButton.onClick.RemoveListener(OnReturnBackButtonClick);
    }

    private void OnReturnBackButtonClick()
    {
        Services.Windows.Close(WindowName.WinWindow);
        Services.GameState.RunTo(Base.GameState.Menu);

        Services.Game.ResetThrows();
    }
}
