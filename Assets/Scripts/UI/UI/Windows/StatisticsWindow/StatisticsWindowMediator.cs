using UI;

public class StatisticsWindowMediator : WindowMediator<StatisticsWindowView, WindowData>
{
    protected override void OnShow()
    {
        base.OnShow();

        View.CutObjCount.text = Services.Statistics.Statistics.ObjectsCut.ToString();
        View.WinsCount.text = Services.Statistics.Statistics.Wins.ToString();
        View.LossesCount.text = Services.Statistics.Statistics.Losses.ToString();
    }
}
