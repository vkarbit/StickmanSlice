using UI;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsWindowView : WindowView
{
    [SerializeField] private Text _cutObjCount;
    [SerializeField] private Text _winsCount;
    [SerializeField] private Text _lossesCount;
    public Text CutObjCount => _cutObjCount;
    public Text WinsCount => _winsCount;
    public Text LossesCount => _lossesCount;
}
