﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SOrca.Flibustiers.Client.Assets.Imported_Systems.Layout
{
  [ExecuteInEditMode]
  public class LayoutElementFitterByGameObject : UIBehaviour
  {
    [SerializeField] private RectTransform _baseTransform;

    protected override void Awake()
    {
      if (_baseTransform == null) _baseTransform = GetComponentInParent<RectTransform>();

      SetSize();
    }

    private void Update()
    {
      SetSize();
    }

    protected override void OnRectTransformDimensionsChange()
    {
      SetSize();
    }

    private void SetSize()
    {
      var rect =  GetComponent<RectTransform>();
      if (rect == null || _baseTransform == null) return;

      if (rect.sizeDelta != _baseTransform.sizeDelta ||
            rect.localPosition != _baseTransform.localPosition ||
            rect.localRotation != _baseTransform.localRotation ||
            rect.anchoredPosition != _baseTransform.anchoredPosition ||
            rect.localScale != _baseTransform.localScale ||
            rect.pivot != _baseTransform.pivot
            )
      {
        rect.anchorMax = _baseTransform.anchorMax;
        rect.anchorMin = _baseTransform.anchorMin;
        rect.SetPivot(_baseTransform.pivot);
        rect.localPosition = _baseTransform.localPosition;
        rect.sizeDelta = _baseTransform.sizeDelta;
        rect.localRotation = _baseTransform.localRotation;
        rect.anchoredPosition = _baseTransform.anchoredPosition;
        rect.localScale = _baseTransform.localScale;
      }
      
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
      SetSize();
    }
#endif
  }
}

public static class RectTransformEx
{
    public static float GetWorldTop(this RectTransform rt)
    {
        return rt.position.y + (1f - rt.pivot.y) * rt.rect.height;
    }

    public static float GetWorldBottom(this RectTransform rt)
    {
        return rt.position.y - rt.pivot.y * rt.rect.height;
    }

    public static float GetWorldLeft(this RectTransform rt)
    {
        return rt.position.x - rt.pivot.x * rt.rect.width;
    }

    public static float GetWorldRight(this RectTransform rt)
    {
        return rt.position.x + (1f - rt.pivot.x) * rt.rect.width;
    }

    public static Bounds GetBoundsWithChildren(this RectTransform @this, bool recursive = true)
    {
        var result = new Bounds(@this.localPosition, @this.GetSize());

        var layoutElementSize = GetLayoutElementSize(@this);
        var layoutGroupSize = GetLayoutGroupSize(@this);
        if (layoutElementSize != Vector2.zero)
            return new Bounds(@this.localPosition, layoutElementSize);
        if (layoutGroupSize != Vector2.zero)
            return new Bounds(@this.localPosition, layoutGroupSize);

        foreach (RectTransform child in @this)
        {
            if (!child.gameObject.activeSelf) continue;

            result.Encapsulate(new Bounds(child.localPosition, child.GetSize()));
        }

        return result;
    }

    public static Vector2 GetSize(this RectTransform @this)
    {
        var size = @this.sizeDelta;

        var layoutElementSize = GetLayoutElementSize(@this);
        var layoutGroupSize = GetLayoutGroupSize(@this);

        if (layoutElementSize != Vector2.zero)
            return layoutElementSize;
        if (layoutGroupSize != Vector2.zero)
            return layoutGroupSize;

        return size;
    }


    public static Vector2 GetLayoutGroupSize(this RectTransform @this)
    {
        var layoutGroup = @this.GetComponent<LayoutGroup>();
        if (layoutGroup != null) return new Vector2(layoutGroup.preferredWidth, layoutGroup.preferredHeight);
        return Vector2.zero;
    }

    public static Vector2 GetLayoutElementSize(this RectTransform @this)
    {
        var layout = @this.GetComponent<LayoutElement>();
        if (layout != null && !layout.ignoreLayout) return new Vector2(layout.minWidth, layout.minHeight);
        return Vector2.zero;
    }

    public static Bounds GetBounds(this RectTransform @this)
    {
        return new Bounds(@this.position, @this.GetSize());
    }

    public static float GetBottom(this RectTransform @this)
    {
        return @this.anchoredPosition.y - @this.GetSize().y * @this.localScale.y * @this.pivot.y;
    }

    public static void ClearChilds(this RectTransform @this)
    {
        foreach (Transform t in @this) Object.Destroy(t.gameObject);
    }

    public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
    {
        if (rectTransform == null) return;

        var size = rectTransform.rect.size;
        Vector2 scale = rectTransform.localScale;
        var deltaPivot = rectTransform.pivot - pivot;
        var deltaPosition = new Vector3(deltaPivot.x * size.x * scale.x, deltaPivot.y * size.y * scale.y);
        rectTransform.pivot = pivot;
        rectTransform.localPosition -= deltaPosition;
    }
}
