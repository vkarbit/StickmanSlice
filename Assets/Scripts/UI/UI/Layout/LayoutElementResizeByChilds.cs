﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SOrca.Flibustiers.Client.Assets.Imported_Systems.Layout
{
  [ExecuteInEditMode]
  [RequireComponent(typeof(LayoutElement))]
  public class LayoutElementResizeByChilds : UIBehaviour
  {
    [SerializeField] private bool setWidth = true;
    [SerializeField] private bool setHeight = true;
    
    [SerializeField] private bool _useMaxHeight = false;

        // [ConditionalField("_useMaxHeight")]
    [SerializeField] private float _maxHeight = float.MaxValue;
    [SerializeField] private bool ignoreInactive = true;

    private RectTransform _rectTransform;
    private LayoutElement _layout;


    protected override void Awake()
    {
      if (_layout == null) _layout = GetComponent<LayoutElement>();
      if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
      UpdateSize();
    }

    public void UpdateSize()
    {
      if (_rectTransform == null || _layout == null) return;

      Bounds bounds = new Bounds();
      if (transform.childCount > 0)
      {
        foreach (RectTransform child in transform)
        {
          if (ignoreInactive && !child.gameObject.activeSelf) continue;

          Bounds childBounds = new Bounds(Vector3.zero, child.rect.size - child.anchoredPosition);
          bounds.Encapsulate(childBounds);
        }
      }

      if (setWidth)
        _layout.preferredWidth = bounds.size.x;

      if (setHeight)
        _layout.preferredHeight = _useMaxHeight ? Math.Min(bounds.size.y, _maxHeight) : bounds.size.y;
    }
  }
}
