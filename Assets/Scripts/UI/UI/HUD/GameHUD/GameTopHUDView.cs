using UI;
using UnityEngine;
using UnityEngine.UI;

public class GameTopHUDView : HudView
{
    [SerializeField] private Text _throwCountText;

    public Text ThrowCountText => _throwCountText;
}
