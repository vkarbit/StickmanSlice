using Base;
using UI;

public class GameTopHUDMediator : HudMediator<GameTopHUDView>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        UpdateThrowCount();

        Services.Game.OnCurrentNumberThrowsChanged += UpdateThrowCount;
    }

    protected override void OnUnMediate()
    {
        base.OnUnMediate();

        Services.Game.OnCurrentNumberThrowsChanged -= UpdateThrowCount;
    }

    private void UpdateThrowCount()
    {
        View.ThrowCountText.text = (Services.Game.MaxNumberThrows - Services.Game.CurrentNumberThrows).ToString();
    }
}
