using UI;
using UnityEngine;
using UnityEngine.UI;

public class MenuHUDView : HudView
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _statisticsButton;

    public Button PlayButton => _playButton;
    public Button StatisticsButton => _statisticsButton;
}
