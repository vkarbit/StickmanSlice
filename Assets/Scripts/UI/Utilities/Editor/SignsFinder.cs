﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Tools
{
    public class SignsFinder : MonoBehaviour
    {
        [SerializeField] private string _folderName;
        [SerializeField] private string _exceptionSigns;
        [SerializeField] private Object _csvFile;
        [SerializeField] private Object _writeTargetFolder;

        private CSVReader _csvReader;

        [ContextMenu("GetAllSigns")]
        public void GetAllSigns()
        {
            string path = GetPathFromFolder(_csvFile);

            if (_csvReader == null)
                _csvReader = new CSVReader();

            List<string> rows = _csvReader.Read(path);

            if (rows == null || rows.Count == 0)
                return;

            //string dataColumn = GetColumnData(files, _targetLanguage);

            //if (string.IsNullOrEmpty(dataColumn))
            //    return;

            //string signs = SortAllSigns(dataColumn);

            string signs = SortSigns(rows);

            string writePath = GetPathFromFolder(_writeTargetFolder);
            writePath += "/" + _folderName + "_" + signs.Length + ".txt";
           
            WriteText(writePath, signs);
            AssetDatabase.Refresh();
        }

        private string GetPathFromFolder(Object obj)
        {
            string path = AssetDatabase.GetAssetPath(obj);
            path = path.Replace("Assets", "");
            return Application.dataPath + path;
        }

        private string SortSigns(List<string> rows)
        {
            string signs = string.Empty;

            for (int i = 0; i < rows.Count; i++)
            {
                for (int j = 0; j < rows[i].Length; j++)
                {
                    bool exception = false;

                    for (int k = 0; k < _exceptionSigns.Length; k++)
                    {
                        if(_exceptionSigns[k] == rows[i][j])
                        {
                            exception = true;
                            break;
                        }
                    }

                    if (!exception && !signs.Contains(rows[i][j].ToString()))
                    {
                        signs += rows[i][j];
                    }
                }
            }

            return signs;
        }

        #region Columns

        private string GetColumnData(List<string> csvFile, string columnName)
        {
            string[] namesColumns = csvFile[0].Split(',');

            bool foundLanguage = false;
            int index = 0;

            for (int i = 0; i < namesColumns.Length; i++)
            {
                if(namesColumns[i].Equals(columnName))
                {
                    index = i;
                    foundLanguage = true;
                    break;
                }
            }

            if(!foundLanguage)
            {
                return null;
            }

            string columnText = string.Empty;

            for (int i = 1; i < csvFile.Count; i++)
            {
                string[] row = CsvParser(csvFile[i]);//csvFile[i].Split(',');
               
                try
                {
                    columnText += row[index];
                }
                catch (System.Exception ex)
                {
                    print(i);
                    print(ex);
                }
            }

            return columnText;
        }

        private string SortAllSigns(string data)
        {
            string signs = string.Empty;

            for (int i = 0; i < data.Length; i++)
            {
                if (!signs.Contains(data[i].ToString()))
                {
                    signs += data[i];
                }
            }

            return signs;
        }

        public string[] CsvParser(string csvText)
        {
            List<string> tokens = new List<string>();

            int last = -1;
            int current = 0;
            bool inText = false;

            while (current < csvText.Length)
            {
                switch (csvText[current])
                {
                    case '"':
                        inText = !inText; break;
                    case ',':
                        if (!inText)
                        {
                            tokens.Add(csvText.Substring(last + 1, (current - last)).Trim(' ', ','));
                            last = current;
                        }
                        break;
                    default:
                        break;
                }
                current++;
            }

            if (last != csvText.Length - 1)
            {
                tokens.Add(csvText.Substring(last + 1).Trim());
            }

            return tokens.ToArray();
        }

        #endregion

        private void WriteText(string path, string data)
        {
            File.WriteAllText(path, data);
        }
    }

    public class CSVReader
    {
        public List<string> Read(string filePath)
        {
            List<string> list = LoadCsvFile(filePath);
            return list;
        }

        private List<string> LoadCsvFile(string filePath)
        {
            var reader = new StreamReader(File.OpenRead(filePath));
            List<string> searchList = new List<string>();

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                searchList.Add(line);
            }
            return searchList;
        }
    }
}



