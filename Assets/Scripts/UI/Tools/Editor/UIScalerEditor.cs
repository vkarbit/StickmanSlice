﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(UIScaler))]
public class UIScalerEditor : Editor
{
    SerializedProperty updateEditor;
    ReorderableList pointsList; 
    
    void OnEnable()
    {
        updateEditor = serializedObject.FindProperty ("_updateInEditor");
        
        pointsList = new ReorderableList(serializedObject, serializedObject.FindProperty("_points"), false, true, false, true);
        
        pointsList.drawElementCallback = DrawListItems;
        pointsList.drawHeaderCallback = DrawHeader;
    }
    
    void DrawHeader(Rect rect)
    {
        EditorGUI.LabelField(new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight), "Target width"); 
        EditorGUI.LabelField(new Rect(rect.x+130, rect.y, 100, EditorGUIUtility.singleLineHeight), "Target size"); 
        EditorGUI.LabelField(new Rect(rect.x+230, rect.y, 100, EditorGUIUtility.singleLineHeight), "Target position"); 
    }
    
    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = pointsList.serializedProperty.GetArrayElementAtIndex(index); // The element in the list
        
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight), 
            element.FindPropertyRelative("ParentWidth"),
            GUIContent.none
        ); 
        
        EditorGUI.PropertyField(
            new Rect(rect.x  + 110, rect.y, 100, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("DeltaSize"),
            GUIContent.none
        ); 
        
        EditorGUI.PropertyField(
            new Rect(rect.x+220, rect.y, 100, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("AnchorPosition"),
            GUIContent.none
        );        
    }

    public override void OnInspectorGUI()
    {
        var uiScaler = (UIScaler)target;
        
        serializedObject.Update();
        
        pointsList.DoLayoutList();
        
        EditorGUILayout.PropertyField (updateEditor, new GUIContent ("Update In Editor"));

        if (!updateEditor.boolValue)
        {
            if (GUILayout.Button("Bake current pos"))
            {
                uiScaler.BakeCurrentInfo();
            }
        }

        serializedObject.ApplyModifiedProperties ();
    }
}

#endif
