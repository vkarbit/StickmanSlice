﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleToGUI : MonoBehaviour
{
    #if !UNITY_EDITOR
    static string myLog = "";
    private string output;
    private string stack;

    void OnEnable()
    {
        Application.logMessageReceived += Log;
    }
     
    void OnDisable()
    {
        Application.logMessageReceived -= Log;
    }
     
    public void Log(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            output = logString;
            stack = stackTrace;
            myLog = output + "\n" + myLog;
            if (myLog.Length > 5000)
            {
                myLog = myLog.Substring(0, 4000);
            }
        }
    }
     
    void OnGUI()
    {
        //if (!Application.isEditor) //Do not display in editor ( or you can use the UNITY_EDITOR macro to also disable the rest)
        // {
            if (!string.IsNullOrEmpty(myLog))
            {
                GUI.contentColor = Color.red;
                myLog = GUI.TextArea(new Rect(10, Screen.height*0.8f, Screen.width - 20, (Screen.height*0.2f)-10), myLog);
            }
        // }
    }
    #endif
}

