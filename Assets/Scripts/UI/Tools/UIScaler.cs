﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(RectTransform))]
public class UIScaler : MonoBehaviour
{
    [System.Serializable]
    public class ScalePoint
    {
        public float ParentWidth;
        public Vector2 DeltaSize;
        public Vector2 AnchorPosition;
    }

    [SerializeField] List<ScalePoint> _points = new List<ScalePoint>();
    [SerializeField] private bool _updateInEditor;

    private RectTransform _parent;
    private RectTransform Parent => Rect.parent.GetComponent<RectTransform>();

    private RectTransform _rect;

    private RectTransform Rect
    {
        get
        {
            if (_rect == null)
                _rect = GetComponent<RectTransform>();

            return _rect;
        }
    }
    
    public void BakeCurrentInfo()
    {
        if (Parent == null || Rect == null) return;

        var rect = Parent.rect;
        var newPoint = new ScalePoint()
        {
            ParentWidth = rect.width/ rect.height,
            DeltaSize = Rect.sizeDelta,
            AnchorPosition = Rect.anchoredPosition
        };

        var foundClosestPoint = false;
        
        for (var i = 0; i < _points.Count; i++)
        {
            if (Mathf.Abs(_points[i].ParentWidth - newPoint.ParentWidth) > 0.05f) continue;
            
            _points[i] = newPoint;

            foundClosestPoint = true;
            
            break;
        }

        if(!foundClosestPoint)
            _points.Add(newPoint);
        
        _points.Sort((e1, e2) =>
        {
            if (e1.ParentWidth > e2.ParentWidth)
            {
                return 1;
            }

            return -1;
        });
    }

    public void Start()
    {
        if ((Application.isEditor && _updateInEditor) || Application.isPlaying)
        {
            CalculateInfo();
        }
    }

    public void Update()
    {
        if (Application.isEditor && _updateInEditor)
        {
            CalculateInfo();
        }
    }

    void CalculateInfo()
    {
        if(_points.Count == 0)
            return;

        // bool resized = false;
        
        var rect = Parent.rect;
        
        var size = rect.width/rect.height;

        for (var i = _points.Count-1; i >= 0 ; i--)
        {
            if (_points[i].ParentWidth > size) continue;
            
            if (i >= _points.Count - 1)
                ApplyInfo(_points[i].DeltaSize, _points[i].AnchorPosition);
            else
                ApplyTwoPoints(_points[i], _points[i + 1], size);

            // resized = true;
            
            return;
        }
        
        // if(!resized)
        
        ApplyInfo(_points[0].DeltaSize, _points[0].AnchorPosition);
    }

    void ApplyTwoPoints(ScalePoint a, ScalePoint b, float currentWidth)
    {
        var max = (b.ParentWidth - a.ParentWidth);
        var current = Mathf.Clamp((currentWidth - a.ParentWidth), 0, max);
        
        var t = current / max;

        var targetSize = Vector2.Lerp(a.DeltaSize, b.DeltaSize, t);
        var targetPosition = Vector2.Lerp(a.AnchorPosition, b.AnchorPosition, t);

        ApplyInfo(targetSize, targetPosition);
    }

    void ApplyInfo(Vector2 size, Vector2 position)
    {
        Rect.sizeDelta = size;
        Rect.anchoredPosition = position;
    }
}
