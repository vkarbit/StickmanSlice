﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PhysicsShadowUI : MonoBehaviour
{
    [Range(0,1)]
    public float Progress;
    public Vector2 Angle;
    public Vector2 DistanceRange;
    public Vector2 TransparencyRange;
    public Vector2 ScaleRange;

    private RectTransform _shadow;
    private Image _image;
    private RectTransform _this;

    private void Awake()
    {
        _this = GetComponent<RectTransform>();
        _shadow = new GameObject(_this.gameObject.name + "_Shadow").AddComponent<RectTransform>();

        CopyRectSettings();
        SetupImage();
    }

    void CopyRectSettings()
    {
        _shadow.SetParent(_this.parent);
        _shadow.localPosition = Vector3.zero;
        _shadow.localScale = Vector3.one;
        _shadow.anchorMin = _this.anchorMin;
        _shadow.anchorMax = _this.anchorMax;
        _shadow.anchoredPosition = _this.anchoredPosition;
        _shadow.sizeDelta = _this.sizeDelta;
    }

    void SetupImage()
    {
        _image = _shadow.gameObject.AddComponent<Image>();
        _image.sprite = _this.GetComponent<Image>().sprite;
        _image.color = Color.black;
    }

    void OnEnable()
    {
        Rearange();          
    }

    public void Rearange()
    {
        _shadow.SetParent(_this.parent);
        _shadow.SetSiblingIndex(_this.GetSiblingIndex());
    }

    public void SetActive(bool enable)
    {
        _shadow.gameObject.SetActive(enable);
    }

    void Update()
    {
        _shadow.position = _this.position + (Vector3)(Angle * GetValue(DistanceRange, Progress));
        _shadow.localScale = _this.localScale * GetValue(ScaleRange, Progress);
        _shadow.rotation = _this.rotation;
        
        var bufferColor = _image.color;
        bufferColor.a = GetValue(TransparencyRange, Progress);
        _image.color = bufferColor;
    }

    float GetValue(Vector2 range, float progress)
    {
        return Mathf.Lerp(range.x, range.y, progress);
    }
}
