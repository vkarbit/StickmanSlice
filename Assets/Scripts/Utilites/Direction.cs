﻿namespace Context.Services.InputService
{
    public enum Direction
    {
        None = 0,
        Right = 1,
        Left = 2
    }
}