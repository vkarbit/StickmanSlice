using Base.Storage;

namespace Base
{
    public abstract class DataService<T, R> : Service
        where T : class, new()
        where R : StorageRoute, new()
    {
        protected IDataStorage<T> Storage { get; private set; }
        protected T Data => Storage.Data;

        protected abstract string FileName { get; }

        protected sealed override void OnInitializeService(ServiceInitializer serviceInitializer)
        {
            base.OnInitializeService(serviceInitializer);

            serviceInitializer.InitializeData += OnInitializeData;
        }

        private void OnInitializeData()
        {
            Storage = new DataStorage<T>(FileName, new R());
        }
    }
}