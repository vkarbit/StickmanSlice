using System;

namespace Base
{
    public class HandlerInitializer
    {
        public event Action<Services> Inject;
        public event Action Initialize;

        public void Run(Services services)
        {
            Inject?.Invoke(services);
            Initialize?.Invoke();
        }
    }
}