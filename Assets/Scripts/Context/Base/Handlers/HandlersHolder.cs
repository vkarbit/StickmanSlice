using System;
using System.Collections.Generic;

namespace Base
{
    public class HandlersHolder : ServicesInjector
    {
        public HandlersHolder(Services services) : base(services)
        {
        }

        private HandlerInitializer _initializer;
        private readonly List<Handler> _handlers = new List<Handler>();

        public void Add(Handler handler)
        {
            _handlers.Add(handler);
        }

        public void Initialize()
        {
            _initializer = new HandlerInitializer();
            foreach (var handler in _handlers)
            {
                handler?.InitializeService(_initializer);
            }
            
            _initializer.Run(Services);
        }

        public void Dispose()
        {
            foreach (var handler in _handlers)
            {
                handler.SafeDispose();
            }
            
            _handlers.Clear();
        }
    }
}