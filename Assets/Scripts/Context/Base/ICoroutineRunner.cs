﻿using System;
using System.Collections;
using UnityEngine;

namespace Base
{
    public interface ICoroutineRunner
    {
        public Coroutine StartCoroutine(IEnumerator coroutine);
        void StopCoroutine(Coroutine movementCoroutine);
        public void DoAfter(Func<bool> condition, Action action);
    }
}