using Base;
using System.Collections.Generic;

namespace GamePlay.Managers
{
    public class ManagersContext
    {
        private readonly List<BaseManager> _managers = new List<BaseManager>();
        public FactoryManager Factories { get; }
        public ManagersContext(Services services)
        {
            Add(Factories = new FactoryManager(services.Resources, services.Game, services.Statistics));
        }

        private void Add(BaseManager manager)
        {
            _managers.Add(manager);
        }

        public void Initialize()
        {
            foreach (var manager in _managers)
            {
                manager.InitializeObject(this);
            }
        }

        public void PostInitialize()
        {
            foreach (var manager in _managers)
            {
                manager.PostInitialize();
            }
        }

        public void Dispose()
        {
            foreach (var manager in _managers)
            {
                manager.Dispose();
            }
        }

    }
}
