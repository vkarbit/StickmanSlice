using Base;
using GamePlay.Managers;

public class FactoryManager : BaseManager
{
    public PlayerFactory PlayerFactory { get; private set; }
    public LocationFactory LocationFactory { get; private set; }

    private readonly ResourcesService _resourcesService;
    private readonly GameService _gameService;
    private readonly StatisticsService _statisticsService;

    public FactoryManager(ResourcesService resourcesService, GameService gameService, StatisticsService statisticsService)
    {
        _resourcesService = resourcesService;
        _gameService = gameService;
        _statisticsService = statisticsService;
    }

    protected override void Initialize()
    {
        base.Initialize();

        InitializeLocationFactory();
        InitializePlayerFactory();
    }

    public override void Dispose()
    {
        
    }

    private void InitializePlayerFactory()
    {
        PlayerFactory = new PlayerFactory(_resourcesService, _gameService, _statisticsService);
    }
    private void InitializeLocationFactory()
    {
        LocationFactory = new LocationFactory(_resourcesService, _gameService);
    }
}
