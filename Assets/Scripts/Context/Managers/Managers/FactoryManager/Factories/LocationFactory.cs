using Base;

public class LocationFactory
{
    private readonly ResourcesService _resourcesService;
    private readonly GameService _gameService;
    public LocationFactory(ResourcesService resourcesService, GameService gameService) 
    {
        _resourcesService = resourcesService;
        _gameService = gameService;

        CreateFactory();
    }

    private void CreateFactory()
    {
        Location location = _resourcesService.Prefabs.GetInstance<Location>(PrefabName.Location);

        location.Initialize(_gameService);
    }
}
