using Base;
using UnityEngine;

public class PlayerFactory
{
    private readonly ResourcesService _resourcesService;
    private readonly GameService _gameService;
    private readonly StatisticsService _statisticsService;

    public PlayerFactory(ResourcesService resourcesService, GameService gameService, StatisticsService statisticsService)
    {
        _resourcesService = resourcesService;
        _gameService = gameService;
        _statisticsService = statisticsService;

        CreatePlayer();
    }

    private void CreatePlayer()
    {
        Player player = _resourcesService.Prefabs.GetInstance<Player>(PrefabName.Player);

        player.Initialize(_gameService, _statisticsService);
    }
}
