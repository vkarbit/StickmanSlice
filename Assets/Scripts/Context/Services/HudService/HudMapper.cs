using UI;
using UI.HUD;

public enum HUDName
{
    None = 0,
    MenuHUD = 1,
    GameHUD = 2,
    DarkeningPanelWindowHud = 3,
}

namespace UIMapper
{
    public class HudMapper : UIMapper<HUDName, HudMediator, HudView>
    {
        public HudMapper()
        {
            Map<MenuHUDMediator>(HUDName.MenuHUD);
            Map<GameTopHUDMediator>(HUDName.GameHUD);
            Map<DarkeningPanelWindowHudMediator>(HUDName.DarkeningPanelWindowHud);
        }
    }
}