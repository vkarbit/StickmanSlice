using System.Collections.Generic;
using Base.HUD;
using UI;
using UnityEngine;

namespace Base
{
    public class HudService : Service
    {
        private HUDProvider _hudProvider;
        private Dictionary<HUDName, HudMediator> _cache;
        private Queue<HudMediator> _actives;

        private readonly HashSet<WindowName> _windowsExceptionDarkeningPanel = new HashSet<WindowName>()
        {
            WindowName.WinWindow,
            WindowName.StatisticsWindow,
            WindowName.LoseWindow,
        };

        private readonly HashSet<WindowName> _windowsExceptionIgnoreEffect = new HashSet<WindowName>()
        {

        };

        protected override void OnInitialize()
        {
            base.OnInitialize();
            
            _hudProvider = new HUDProvider(Services);
            _cache = new Dictionary<HUDName, HudMediator>();
            _actives = new Queue<HudMediator>();

            Services.Windows.Opened += OnWindowOpen;
            Services.Windows.Closed += OnWindowClose;
            Services.Windows.ClosedAll += TryShow;
            Services.GameState.TransitionStart += OnStateChange;
        }

        private void OnWindowOpen(WindowName window)
        {
            if (_windowsExceptionDarkeningPanel.Contains(window))
            {
                Services.HUD.Show(HUDName.DarkeningPanelWindowHud);

                Services.DarkeningPanel.Show();
                return;
            }

            if (_windowsExceptionIgnoreEffect.Contains(window))
            {
                return;
            }

            HideAll();
        }

        private void OnWindowClose(WindowName window)
        {
            if (_windowsExceptionDarkeningPanel.Contains(window))
            {
                Services.DarkeningPanel.Hide();

                return;
            }

            if (_windowsExceptionIgnoreEffect.Contains(window))
            {
                return;
            }

            HideAll();
        }

        private void OnStateChange(GameState state)
        {
            TryShow(state);
        }

        private void TryShow()
        {
            TryShow(Services.GameState.Current);
        }

        private void TryShow(GameState state)
        {
            HideAll();
            
            if (state == GameState.Menu)
            {
                Show(HUDName.MenuHUD);
                return;
            }
            
            if (state == GameState.Game)
            {
                Show(HUDName.GameHUD);
                return;
            }
        }

        public void ShowHud(HUDName name)
        {
            HideAll();

            Show(name);
        }

        private void Show(HUDName name, HudData data = null)
        {
            HudMediator hud;

            if (_cache.ContainsKey(name))
            {
                hud = _cache[name];
            }
            else
            {
                hud = _hudProvider.Get(name);

                if (hud == null)
                {
                    Debug.LogError("no windowType ");
                    return;
                }

                _cache.Add(name, hud);
            }

            hud.SetData(data);
            hud.Show();
            
            _actives.Enqueue(hud);
        }

        private void HideAll()
        {
            while (_actives.Count > 0)
            {
                Hide(_actives.Dequeue().Name);
            }
        }

        public void Hide(HUDName hudName)
        {
            if (_cache.ContainsKey(hudName))
            {
                _cache[hudName].Hide();
            }
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            
            Services.Windows.Opened -= OnWindowOpen;
            Services.Windows.Closed -= OnWindowClose;
            Services.Windows.ClosedAll -= TryShow;
            Services.GameState.TransitionStart -= OnStateChange;
        }
    }
}