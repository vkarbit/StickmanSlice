using System;
using UnityEngine;

namespace Base
{
    public class UnityBridgeBehaviour : MonoBehaviour
    {
        private event Action<bool> _onFocus;
        private event Action<bool> _onPause;
        private event Action _onTick;
        private event Action _onFixedTick;
        private event Action _onQuit;
        
        public void Initialize(Action onTick, Action onFixedTick, Action<bool> onFocus, Action<bool> onPause, Action onQuit)
        {
            _onTick = onTick;
            _onFixedTick = onFixedTick;
            _onFocus = onFocus;
            _onPause = onPause;
            _onQuit = onQuit;
        }

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private void FixedUpdate()
        {
            _onFixedTick?.Invoke();
        }

        private void Update()
        {
            _onTick?.Invoke();
        }

        private void OnApplicationFocus(bool focus)
        {
            _onFocus?.Invoke(focus);
        }

        private void OnApplicationPause(bool pause)
        {
            _onPause?.Invoke(pause);
        }

        private void OnApplicationQuit()
        {
            _onQuit?.Invoke();
        }
    }
}