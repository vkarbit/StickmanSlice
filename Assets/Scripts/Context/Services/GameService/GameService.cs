using Base;
using DG.Tweening;
using System;
using UnityEngine;

public class GameService : Service
{
    public int MaxNumberThrows { get; private set; }
    public int CurrentNumberThrows { get; private set; }
    public Action OnCurrentNumberThrowsChanged;
    private Location _location;

    protected override void OnInitialize()
    {
        base.OnInitialize();

        CurrentNumberThrows = 0;

        var config = Services.Resources.ScriptableObjects.Get(FolderName.StaticData, "GameConfig") as GameConfig;

        MaxNumberThrows = config.MaxNumberThrows;
    }
    public void DoThrows()
    {
        CurrentNumberThrows += 1;
        OnCurrentNumberThrowsChanged?.Invoke();

        if(CurrentNumberThrows == MaxNumberThrows)
        {
            EndLevel();
        }
    }
    public void ResetThrows()
    {
        CurrentNumberThrows = 0;
        OnCurrentNumberThrowsChanged?.Invoke();
    }

    public bool CanDoThrow()
    {
        return CurrentNumberThrows < MaxNumberThrows;
    }

    public void EndLevel()
    {
        DOVirtual.DelayedCall(3f, () =>
        {
            if (_location.CheckCubesOnEndThrows())
            {
                Services.Windows.Open(WindowName.WinWindow);
                return;
            }
            Services.Windows.Open(WindowName.LoseWindow);
        });
    }


    public void GetLocation(Location location)
    {
        _location = location;
    }
    protected override void BindHandlers()
    {
        base.BindHandlers();

        Handlers.Add(_ = new MenuInitializationHandler());
        Handlers.Add(_ = new GameInitializationHandler());
    }
}
