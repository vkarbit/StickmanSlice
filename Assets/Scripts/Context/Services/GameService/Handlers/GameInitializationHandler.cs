using Base;

public class GameInitializationHandler : Handler
{
    protected override void OnInitialize()
    {
        base.OnInitialize();

        Services.GameState.TransitionRequiredAction += TryInitializeGame;
    }

    private void TryInitializeGame(GameState state)
    {
        if (state == GameState.Game)
        {
            InitializeGame();
        }
    }

    private void InitializeGame()
    {
        GameInitializer gameInitializer = Services.Resources.Prefabs.GetInstance<GameInitializer>(PrefabName.GameInitializer);

        gameInitializer.Initialize(Services);
    }

    protected override void OnDispose()
    {
        base.OnDispose();

        Services.GameState.TransitionRequiredAction -= TryInitializeGame;
    }
}
