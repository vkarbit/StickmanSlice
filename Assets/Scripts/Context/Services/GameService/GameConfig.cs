using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObjects/Config/GameConfig")]
public class GameConfig : ScriptableObject
{
    [SerializeField] private int _maxNumberThrows;
    public int MaxNumberThrows => _maxNumberThrows;
}
