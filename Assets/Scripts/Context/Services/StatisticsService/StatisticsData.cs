public class StatisticsData
{
    public StatisticsData()
    {
        ObjectsCut = 0;
        Wins = 0;
        Losses = 0;
    }

    public int ObjectsCut { get; set; }
    public int Wins { get; set; }
    public int Losses { get; set; }
}
