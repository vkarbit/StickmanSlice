using Base;
using System;

public class StatisticsHandler : DataHandler<StatisticsData>
{
    public StatisticsHandler(StatisticsData data, Action saveCallback) : base(data, saveCallback)
    {
    }

    public int ObjectsCut => Data.ObjectsCut;
    public int Wins => Data.Wins;
    public int Losses => Data.Losses;

    public void AddCutObj()
    {
        Data.ObjectsCut += 1;
        Save();
    }
    public void AddWin()
    {
        Data.Wins += 1;
        Save();
    }
    public void AddLose()
    {
        Data.Losses += 1;
        Save();
    }

}
