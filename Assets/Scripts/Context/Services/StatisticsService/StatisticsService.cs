using Base;
using Base.Storage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsService : DataService<StatisticsData, ProfileRoute>
{
    public StatisticsHandler Statistics { get; private set; }

    protected override string FileName => "StatisticsData";

    protected override void BindHandlers()
    {
        base.BindHandlers();

        Handlers.Add(Statistics = new StatisticsHandler(Data, Storage.Save));
    }
}
