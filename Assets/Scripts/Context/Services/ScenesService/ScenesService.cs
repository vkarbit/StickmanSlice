namespace Base
{
    public class ScenesService : Service
    {
        public SceneName Current { get; private set; }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            Current = SceneName.Entrance;
            Services.GameState.SceneLoaded += Loaded;
        }

        private void Loaded(SceneName scene)
        {
            Current = scene;
            //UnityEngine.Debug.Log("[SceneNavigator] Scene loaded: " + scene);
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            
            Services.GameState.SceneLoaded -= Loaded;
        }
    }
}