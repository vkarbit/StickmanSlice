using UI;

namespace UIMapper
{
    public class WindowMapper : UIMapper<WindowName, WindowMediator, WindowView>
    {
        public WindowMapper()
        {
            Map<WinWindowMediator>(WindowName.WinWindow);
            Map<StatisticsWindowMediator>(WindowName.StatisticsWindow);
            Map<LoseWindowMediator>(WindowName.LoseWindow);
        }
    }
}