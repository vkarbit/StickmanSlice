using UI;
using UIMapper;

namespace Base
{
    public class WindowsProvider : ServicesInjector
    {
        private readonly WindowMapper _map = new WindowMapper();

        public WindowsProvider(Services services) : base(services)
        {
        }

        public WindowMediator Get(WindowName window)
        {
            if (Services.Resources.Windows.TryGetInstance(window, out var go))
            {
                Services.UILayers.Add(UILayer.POPUP, go);
                var mediator = _map.Get(window, go, Services);
                mediator.SetName(window);

                return mediator;
            }

            return null;
        }
    }
}