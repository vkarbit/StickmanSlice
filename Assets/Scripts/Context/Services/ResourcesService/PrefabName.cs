public enum PrefabName
{
    None = 0,
    GameInitializer = 1,
    Player = 2,
    Location = 3,
}