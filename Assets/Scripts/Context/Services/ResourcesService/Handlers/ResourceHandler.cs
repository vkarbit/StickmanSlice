using System.Collections.Generic;
using UnityEngine;

namespace Base.Handlers
{
    public abstract class ResourceHandler : Handler
    {
        private readonly Dictionary<string, GameObject> _cache = new Dictionary<string, GameObject>();
        
        protected abstract string Path { get; }

        protected GameObject Get(string key)
        {
            if (_cache.ContainsKey(key) && _cache[key] != null)
            {
                return _cache[key];
            }

            var container = Load<PrefabContainer>(System.IO.Path.Combine(Path, key));
            if (container == null)
            {
                Debug.LogError($"PrefabRefContainer {key} not exist");
                return null;
            }

            if (container.Prefab == null)
            {
                Debug.LogError($"Prefab {key} == null");
                return null;
            }

            _cache.Add(key, container.Prefab);

            return container.Prefab;
        }

        protected T Load<T>(string path) where T : Object
        {
            return Resources.Load<T>(path);
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            
            _cache.Clear();
        }
    }
}