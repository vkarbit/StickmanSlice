using UnityEngine;

namespace Base.Handlers
{
    public class ResourceSpritesHandler : Handler
    {
        private const string SPRITE_PATH = "ScriptableGamePlay";

        public Sprite Get(string name)
        {
            try
            {
                var so = Resources.Load<UnityEngine.Object>(SPRITE_PATH + "/" + name.ToUpper());

                //return ((SpriteScriptableObject)so).Sprite;
                return null;
            }
            catch
            {
                Debug.LogError("no sprite " + name);
            }

            return null;
        }
    }
}