using UnityEngine;

namespace Base.Handlers
{
    public class ResourceScriptableObjectsHandler : Handler
    {
        private const string SCRIPTABLE_OBJECTS_PATH = "ScriptableObjects";

        public ScriptableObject Get(FolderName folderName, string name)
        {
            try
            {
                var so = Resources.Load<UnityEngine.Object>(SCRIPTABLE_OBJECTS_PATH + "/" + folderName + "/" + name);
                
                return so as ScriptableObject;
            }
            catch
            {
                Debug.LogError("no Object " + name);
            }

            return null;
        }
    }
}