using Processes.SplashScreen;

namespace Base.GameStates
{
    public class MenuToGame : GameStateTransition
    {
        protected override void OnStart()
        {
            base.OnStart();

            Add(new ShowSplashScreen(SplashScreenName.Default, SplashScreen));
        }

        protected override void OnEnd()
        {
            base.OnEnd();

            Add(new MoveToScene(SceneName.Game, OnSceneLoaded));
            Add(new WaitGameInitialization(OnTransitionRequiredAction));
            Add(new HideSplashScreen(SplashScreen));
        }
    }
}