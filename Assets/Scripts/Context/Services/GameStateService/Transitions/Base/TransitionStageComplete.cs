using System;

namespace Base.GameStates
{
    public class TransitionStageComplete : Process
    {
        public TransitionStageComplete(Action complete) : base(complete)
        {
        }

        protected override bool OnRun()
        {
            return true;
        }
    }
}