using System;

namespace Base.GameStates
{
    public abstract class GameStateTransition : Process
    {
        public event Action<SceneName> SceneLoaded;
        public event Action TransitionStart;
        public event Action TransitionRequiredAction;
        public event Action TransitionEnd;
        
        protected SplashScreenHandler SplashScreen { get; private set; }

        public void InjectSplashScreen(SplashScreenHandler splashScreen)
        {
            SplashScreen = splashScreen;
        }

        protected sealed override bool OnRun()
        {
            OnStart();
            Add(new TransitionStageComplete(OnStartCallback));
            OnEnd();
            Add(new TransitionStageComplete(OnEndCallback));

            return true;
        }

        protected virtual void OnStart()
        {

        }

        private void OnStartCallback()
        {
            TransitionStart?.Invoke();
        }

        protected virtual void OnEnd()
        {
        }

        private void OnEndCallback()
        {
            TransitionEnd?.Invoke();
        }

        protected void OnSceneLoaded(SceneName scene)
        {
            SceneLoaded?.Invoke(scene);
        }

        protected void OnTransitionRequiredAction()
        {
            TransitionRequiredAction?.Invoke();
        }
    }
}