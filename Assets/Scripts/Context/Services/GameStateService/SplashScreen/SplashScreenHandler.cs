using System;
using UI;
using UnityEngine;

namespace Base
{
    public class SplashScreenHandler : Handler
    {
        private SplashScreenMediator _current;
        private SplashScreenProvider _provider;

        protected override void OnInitialize()
        {
            base.OnInitialize();
            
            _provider = new SplashScreenProvider(Services);
        }

        public void Open(SplashScreenName splash, Action callback)
        {
            _current = _provider.Get(splash);
            if (_current == null) return;

            _current.Shown += callback;
            _current.Show();
        }

        public void Close(Action callback)
        {
            if (_current == null)
            {
                callback?.Invoke();
                return;
            }

            _current.Hided += callback;
            _current.Hide();
            _current = null;
        }
    }
}