using UI;

namespace UIMapper
{
    public class SplashScreenMapper : UIMapper<SplashScreenName, SplashScreenMediator, SplashScreenView>
    {
        public SplashScreenMapper()
        {
            Map<StartSplashScreenMediator>(SplashScreenName.Start);
            Map<DefaultSplashScreenMediator>(SplashScreenName.Default);
        }
    }
}