﻿using UnityEngine;

namespace Base
{
    public class UILayersService : Service
    {
        private UILayers _uiLayers;
        public Camera UICamera => _uiLayers.UICamera;

        public void Install(UILayers uiLayers)
        {
            _uiLayers = uiLayers;
        }

        public void Add(UILayer layer, GameObject go)
        {
            _uiLayers.GetLayer(layer).Add(go);
        }

        public UICanvasLayer Get(UILayer layer)
        {
            return _uiLayers.GetLayer(layer);
        }

        public Vector2 ScreenToCanvasPosition(Vector2 screenPosition)
        {
            var pos = _uiLayers.UICamera.ScreenToWorldPoint(screenPosition);
            
            return _uiLayers.UICanvas.GetComponent<RectTransform>().InverseTransformVector(pos);
        }
    }
}