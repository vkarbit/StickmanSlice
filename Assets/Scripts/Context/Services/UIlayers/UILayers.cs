﻿using UnityEngine;

namespace Base
{ 
    public interface IUILayers
    {
        UICanvasLayer GetLayer(UILayer layer);
    }

    public class UILayers : MonoBehaviour, IUILayers
    {
        [SerializeField] private Canvas _generalCanvas;
        [SerializeField] private Camera _uiCamera;
        [SerializeField] private GameObject[] _canvases;

        private UICanvasLayer[] _layers;

        public Camera UICamera => _uiCamera;

        public Canvas UICanvas => _generalCanvas;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _layers = new UICanvasLayer[_canvases.Length];
        }

        public UICanvasLayer GetLayer(UILayer layer)
        {
            var result = _layers[(int)layer];
            if (result == null)
            {
                _layers[(int)layer] = result = new UICanvasLayer(GetCanvas(layer));
            }
            return result;
        }

        private GameObject GetCanvas(UILayer index)
        {
            var canvas = _canvases[(int)index];
            if (canvas == null)
            {
                _canvases[(int)index] = canvas = new GameObject(index.ToString());
            }
            return canvas;
        }
    }

    public enum UILayer
    {
        HUD,
        WINDOWS,
        POPUP,
        TOOLTIP,
        NOTIFICATION,
        TUTORIAL,
        OVERLAY,
    }
    
    public class UICanvasLayer
    {
        private readonly GameObject _canvas;

        public UICanvasLayer(GameObject canvas)
        {
            _canvas = canvas;
        }

        public void Add(GameObject gameObject)
        {
            gameObject.transform.SetParent(_canvas.transform, false);
        }

        public void Add(Component component)
        {
            component.transform.SetParent(_canvas.transform, false);
        }

    }
}
