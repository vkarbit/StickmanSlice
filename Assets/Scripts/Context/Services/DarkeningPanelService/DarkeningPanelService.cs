using Base;
using System;

public class DarkeningPanelService : Service
{

    #region ACTIONS

    private readonly OneListener _openListener = new OneListener();

    public event Action Opened
    {
        add { _openListener.Add(value); }
        remove { _openListener.Remove(value); }
    }

    private readonly OneListener _closeListener = new OneListener();

    public event Action Closed
    {
        add { _closeListener.Add(value); }
        remove { _closeListener.Remove(value); }
    }

    #endregion

    public void Show()
    {
        _openListener.SafeInvoke();
    }

    public void Hide()
    {
        _closeListener.SafeInvoke();
    }
}
