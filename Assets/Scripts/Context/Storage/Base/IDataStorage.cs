namespace Base.Storage
{
    public interface IDataStorage<out T> where T : class
    {
        T Data { get; }
        
        void Save();

        T Load();
    }
}