using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Base.Storage
{
    public abstract class Storage<TFile> where TFile : class
    {
        private readonly StorageRoute _route;

        protected Storage(StorageRoute route)
        {
            _route = route;
        }

        protected string LoadDirectory => Path.Combine(_route.LoadPath, _route.Folder);
        protected string SaveDirectory => Path.Combine(_route.SavePath, _route.Folder);

        private string FullPath(string directoryPath, string file)
        {
            return $"{directoryPath}/{file}.{_route.Extension}";
        }

        #region SERIALIZE

        protected virtual string Serialize<T>(T jsonObject)
        {
            return JsonConvert.SerializeObject(jsonObject, Formatting.None, new JsonSerializerSettings
            {
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            });
        }

        #endregion

        #region DESERIALIZE

        protected virtual T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        #endregion

        #region LOAD

        public virtual bool TryLoad(string file, out TFile obj)
        {
            return (obj = Load<TFile>(file)) != null;
        }

        public virtual TFile Load(string file)
        {
            return Load<TFile>(file);
        }

        protected T Load<T>(string file) where T : class
        {
            try
            {
                return Deserialize<T>(LoadAtPath(FullPath(LoadDirectory, file)));
            }
            catch
            {
                return null;
            }
        }

        private string LoadAtPath(string path)
        {
            if (File.Exists(path))
                return File.ReadAllText(path);

#if UNITY_ANDROID && !UNITY_EDITOR
            UnityEngine.WWW www = new UnityEngine.WWW(path);
            while (!www.isDone){}
            
            return www.text;
#endif

            return string.Empty;
        }

        #endregion

        #region SAVE

        public virtual void Save(string file, TFile data)
        {
            Save(SaveDirectory, file, Serialize(data));
        }

        protected void Save(string directory, string file, string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return;
            }

            var path = FullPath(directory, file);
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            try
            {
                WriteData(path, data);
            }
            catch (Exception e)
            {
                Directory.CreateDirectory(directory);
                WriteData(path, data);
            }
        }

        private void WriteData(string path, string data)
        {
            using (FileStream fs = File.Create(path))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(data);
                fs.Write(info, 0, info.Length);
            }
        }

        #endregion
    }
}