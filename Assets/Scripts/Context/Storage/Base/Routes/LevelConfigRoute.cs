using UnityEngine;

namespace Base.Storage
{
    public class LevelConfigRoute : StorageRoute
    {
        public override string LoadPath => Application.streamingAssetsPath;
        public override string SavePath => Application.streamingAssetsPath;
        public override string Folder => "Levels";
    }
}