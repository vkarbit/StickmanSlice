using System;

namespace Base.Storage
{
    public class DataStorage<T> : Storage<T>, IDataStorage<T> where T : class, new()
    {
        private readonly OneListener _onSaveData = new OneListener();
        public event Action OnSaveData
        {
            add => _onSaveData.Add(value);
            remove => _onSaveData.Remove(value);
        }

        public DataStorage(string fileName, StorageRoute route) : base(route)
        {
            _state = new EmptyState(this, ChangeState);
            FileName = fileName;
        }

        public string FileName { get; }

        public T Data => _state.Data;

        private State _state;

        private void ChangeState(State state)
        {
            _state = state;
        }

        public void Save()
        {
            base.Save(FileName, Data);
            _onSaveData.SafeInvoke();
        }

        public void Save(T data)
        {
            base.Save(FileName, data);
            _onSaveData.SafeInvoke();
        }

        public T Load()
        {
            return Load(FileName);
        }

        private abstract class State
        {
            protected readonly IDataStorage<T> Storage;
            private event Action<State> _stateChange;

            protected State(IDataStorage<T> storage, Action<State> stateChange)
            {
                Storage = storage;
                _stateChange = stateChange;
            }

            public abstract State Initialize();

            public abstract T Data { get; }

            protected void ChangeState(State state)
            {
                _stateChange?.Invoke(state);
            }
        }

        private class EmptyState : State
        {
            public EmptyState(IDataStorage<T> storage, Action<State> stateChange) : base(storage, stateChange)
            {
            }

            public override T Data
            {
                get
                {
                    var work = new WorkState(Storage, null).Initialize();
                    ChangeState(work);

                    return work.Data;
                }
            }

            public override State Initialize()
            {
                return this;
            }
        }

        private class WorkState : State
        {
            public WorkState(IDataStorage<T> storage, Action<State> stateChange) : base(storage, stateChange)
            {
            }

            private T _data;
            public override T Data => _data;

            public override State Initialize()
            {
                _data = Storage.Load() ?? new T();

                return this;
            }
        }
    }
}